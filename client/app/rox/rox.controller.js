'use strict';

angular.module('rox')
  .controller('RoxCtrl', function ($scope, $document, $q, $timeout, $log, ngAudio) {
    $scope.sound = ngAudio.load('sounds/typewriter_key.mp3');
    $scope.sound2 = ngAudio.load('sounds/typewriter_line_break.mp3');
    $scope.nauru = ngAudio.load('sounds/nauru.mp3');

    $scope.title = '';
    $scope.text = '';
    new Image().src='images/troll-face.png';
    function write(element, str, speed) {
        var dfd = $q.defer();
        var len = str.length;
        var i = 0;
        var interval = setInterval(
          function() {
            $scope.$apply(function() {
              $scope.sound.play();
              var chr = str[i++];
              $scope[element] += chr;
              if (i === len) {
                clearInterval(interval);
                dfd.resolve();
              }
            });
          },
          speed
        );
        //console.log('Create promise');
        return dfd.promise;
    }
    $q.when(write('title', 'Hello', 100)).then(function() {
      $scope.sound2.play();
      $timeout(
        function(){
          $q.when(
            write('text', 'This page is way more cooler than http://rikkola.net/...', 50)
          ).then(function() {
            $timeout(
              function(){
                $q.when(
                  write('text', ' or even http://turkia.fi/', 50)
                ).then(function() {
                  $timeout(function() {
                    $scope.hideFace = false;
                    $scope.nauru.play();
                  }, 500);
                });
              },
              500
            );
          });
        },
        500
      );
    }
   );
    $scope.hideFace = true;
  });

