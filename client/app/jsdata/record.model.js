'use strict';
angular.module('app.models')
  .factory('Record', function(DS) {
    return DS.defineResource({
      name: 'record'
    });
  })
;
