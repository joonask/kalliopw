'use strict';
angular.module('app.models')
  .factory('Track', function(DS) {
    return DS.defineResource({
      name: 'track'
    });
  })
;
