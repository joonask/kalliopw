'use strict';
angular.module('app.models')
  /**
   * jshint:ignore
   */
  .run(function(Vehicle, History) {})
  .factory('Vehicle', function(DS) {
    return DS.defineResource({
      name: 'vehicle',
      relations: {
        hasMany: {
          history: {
            localField: 'history',
            foreignKey: 'vehicle_id'
          }
        }
      },
      computed: {
        registrationDateObj: ['registrationDate', function (date) {
          return new Date(date);
        }]
      }
    });
  })
  .factory('History', function(DS) {
    return DS.defineResource({
      idAttribute: 'id',
      name: 'history',
      relations: {
        belongsTo: {
          vehicle: {
            localField: 'vehicle',
            localKey: 'id'
          }
        }
      },
      computed: {
        dateObj: ['date', function (date) {
          if (date) {
            return new Date(date);
          }
        }]
      }
    })
  })
  .factory('Image', function(DS) {
    return DS.defineResource({
      idAttribute: 'id',
      name: 'image',
      relations: {
        belongsTo: {
          vehicle: {
            localField: 'vehicle',
            localKey: 'id'
          }
        }
      }
    });
  })
;
