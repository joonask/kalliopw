'use strict';
angular.module('app.models')
  .factory('User', function(DS) {
    return DS.defineResource({
      idAttribute: 'uid',
      name: 'users'
    });
  })
;
