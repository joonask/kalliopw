(function() {
  angular.module('demo', ['vehicle'])
    .config(routes);

  function routes($stateProvider) {
      $stateProvider
        .state('demo', {
          url: '/demo',
          templateUrl: 'app/demo/demo.html',
          controller: 'DemoController'
        })
  }

})();
