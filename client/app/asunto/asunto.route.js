'use strict';

angular.module('asunto')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.asunto', {
        url: '/asunto',
        templateUrl: 'app/asunto/asunto.html',
        controller: 'AsuntoCtrl'
      });
  });
