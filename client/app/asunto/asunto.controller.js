'use strict';

angular.module('asunto')
  .controller('AsuntoCtrl', function ($scope) {
    $scope.myInterval = 5000;
    $scope.active = 0;
    var index = 0;
    $scope.slides = [
      {image: 'assets/images/IMG_20130615_222508.jpg', text: 'Keittiö', id: index++},
      {image: 'assets/images/IMG_20130615_222542.jpg', text: 'Keittiö', id: index++},
      {image: 'assets/images/IMG_20130615_222629.jpg', text: 'Keittiö', id: index++},
      {image: 'assets/images/IMG_20130615_224918.jpg', text: 'WC', id: index++},
      {image: 'assets/images/IMG_20130615_212517.jpg', text: 'Olohuone', id: index++},
      {image: 'assets/images/IMG_20130615_212436.jpg', text: 'Olohuone', id: index++}
    ];
  });

