'use strict';

angular.module('calculator', ['LocalStorageModule'])
  .config(function (localStorageServiceProvider) {
  localStorageServiceProvider
    .setPrefix('calculator');
});
