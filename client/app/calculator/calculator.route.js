'use strict';

angular.module('calculator')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.calculator', {
        url: '/polttoainelaskuri',
        template: '<calculator></calculator>'
      });
  });
