'use strict';
angular.module('calculator')
  .directive('calculator', function() {
    return {
      restrict: 'E',
      templateUrl: 'app/calculator/calculator.html',
      scope: {},
      controller: function($scope, localStorageService) {

        $scope.price = localStorageService.get('price')||'';
        $scope.travel = localStorageService.get('travel')||'';
        $scope.consume = localStorageService.get('consume')||'';

        $scope.$watch('consume', function(changed) {
          if (_.isNumber(changed) && changed >= 0) {
            return localStorageService.set('consume', changed);
          }
        });

        $scope.$watch('price', function(changed) {
          if (_.isNumber(changed) && changed > 0) {
            return localStorageService.set('price', changed);
          }
        });

        $scope.$watch('travel', function(changed) {
          if (_.isNumber(changed) && changed > 0) {
            return localStorageService.set('travel', changed);
          }
        });
      }
    };
  });
