(function(angular, undefined) {
'use strict';

angular.module('qctusApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);