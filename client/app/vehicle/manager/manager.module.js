'use strict';

angular.module('vehicle.manager', ['angularMoment', 'ui.router', 'ui.bootstrap.datepicker', 'app.models', 'vehicle']);
