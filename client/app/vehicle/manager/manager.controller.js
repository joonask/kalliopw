'use strict';

angular.module('vehicle.manager')
  .controller('ManagerCtrl', function($q, $scope, $log, vehicles, Vehicle) {
    $scope.vehicles = vehicles;
    $scope.addVehicle = function() {
      var vehicle = {
        make: 'vehicle',
        description: ''
      };
      Vehicle.create(vehicle).then(function(vehicle) {
        $scope.vehicles.push(vehicle);
      });
    };
  })
;
