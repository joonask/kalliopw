'use strict';

angular.module('vehicle.manager')
  .controller('HistoryController', function(History, $log) {
    this.delete = function(id) {
      $log.debug('delete history ' + id);
      History.destroy(id);
    };
  })
;
