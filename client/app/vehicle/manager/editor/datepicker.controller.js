'use strict';

angular.module('vehicle.manager')
  .controller('DatepickerCtrl', function() {
    this.defaultDate = new Date();
    this.format = 'dd.MM.yyyy';
    this.altInputFormats = ['dd.MM.yyyy'];
    this.dateOptions = {
      formatYear: 'yy',
      maxDate: new Date(2020, 5, 22),
      minDate: new Date(1970, 5, 22),
      startingDay: 1,
      initDate: new Date()
    };

    this.popup = false;
    this.open = function () {
      this.popup = true;
    };
  })
;
