'use strict';
angular.module('vehicle.manager')
  .directive('vehicleHistory', function() {
    return {
      restrict: 'E',
      controller: 'HistoryController as ctrl',
      templateUrl: 'app/vehicle/manager/editor/history.html',
      scope: {
        history: '='
      }
    };
  })
;
