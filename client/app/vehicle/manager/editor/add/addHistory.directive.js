angular.module('vehicle.manager')
  .controller('AddHistoryModalController', function() {
    var $ctrl = this;
    $ctrl.history = {};


    $ctrl.ok = function () {
      $uibModalInstance.close($ctrl.history);
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
