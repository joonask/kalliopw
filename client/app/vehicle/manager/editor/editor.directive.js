'use strict';
angular.module('vehicle.manager')
.directive('vehicleEditor', function() {
  return {
    restrict: 'E',
    templateUrl: 'app/vehicle/manager/editor/editor.html',
    controller: 'VehicleEditorCtrl',
    controllerAs: 'ctrl',
    scope: {
      vehicle: '='
    }
  };
})
;
