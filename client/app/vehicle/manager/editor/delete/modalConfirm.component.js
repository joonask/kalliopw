angular.module('vehicle'
).component('modalConfirmComponent', {
  templateUrl: 'app/vehicle/manager/editor/delete/deleteModal.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function() {
    var $ctrl = this;

    $ctrl.$onInit = function () {
      $ctrl.vehicle = $ctrl.resolve.vehicle;
    };

    $ctrl.ok = function() {
      $ctrl.close();
    };

    $ctrl.cancel = function() {
      $ctrl.dismiss();
    };
  }
});
