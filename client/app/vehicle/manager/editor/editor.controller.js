'use strict';

angular.module(
  'vehicle.manager'
).controller(
  'VehicleEditorCtrl',
  function(
    $scope, $state, $filter, $timeout, $log,
    Vehicle, History, moment, Image, filepickerService,
    $uibModal
  ) {
    filepickerService.setKey('AcNGVrs5MQjqghMfqsWPNz');
    var ctrl = this;
    this.vehicle = $scope.vehicle;
    this.vehicle.description = $filter('br2nl')(this.vehicle.description);
    this.format = 'dd.MM.yyyy';
    this.opened = false;
    this.alerts = [];
    this.popup = {
      opened: false
    };
    this.open = function() {
      this.popup.opened = true;
    };
    this.closeAlert = function(index) {
      ctrl.alerts.splice(index, 1);
    };

    this.dateChanged = function(dt) {
      this.vehicle.registrationDate = moment(dt).format('YYYY-MM-DD');
    };

    $scope.hideBasics = true;
    $scope.hideHistory = true;
    this.save = function() {
      _.each(this.vehicle.history, function(history) {
        if (History.hasChanges(history.id)) {
          history.date = moment(history.dateObj).format('YYYY-MM-DD');
          History.save(history.id).then(function() {
            $log.debug('vehicle history saved ' + history.id);
            ctrl.alerts.push({ type: 'success', msg: 'Ajoneuvohistoria päivitetty' });
          });
        }
      });

      if (this.vehicle.id && Vehicle.hasChanges(this.vehicle.id)) {
        // have to remove angular $$hashKey etc shit from the object
        var vehicle = JSON.parse(angular.toJson(this.vehicle));
        vehicle.description = $filter('nl2br')(vehicle.description);
        vehicle = Vehicle.createInstance(vehicle);

        Vehicle.update(vehicle.id, vehicle).then(function (veh) {
          $log.debug('vehicle saved', veh);
          ctrl.alerts.push({ type: 'success', msg: 'Ajoneuvon tiedot päivitetty' });
          ctrl.vehicle.description = $filter('br2nl')(ctrl.vehicle.description);
        }, function (err) {
          $log.error('error while saving vehicle', err);
          ctrl.alerts.push({ type: 'danger', msg: 'Ajoneuvotietojen päivitys epäonnistui' });
          throw err;
        });
      } else {
        Vehicle.create(this.vehicle).then(function() {
          ctrl.alerts.push({type: 'success', msg: 'Uusi ajoneuvo luotu'});
        });

      }
    };

    this.delete = function() {
      var vehicle = this.vehicle;
      $uibModal.open({
        animation: false,
        component: 'modalConfirmComponent',
        resolve: {
          vehicle: function () {
            return vehicle
          }
        }
      }).result.then(reallyDelete);
      // TODO rumasti kaikki controllerissa... toimii (tm)
      function reallyDelete() {

        var destroyParams = {
          where: {
            vehicle_id: {
              '==': vehicle.id
            }
          }
        };
        History.destroyAll(destroyParams).then(function () {
          $log.info('vehicle ' + vehicle.id + ' history removed');
        }, function (err) {
          $log.info('vehicle ' + vehicle.id + ' history error ', err);
        });

        Image.destroyAll(destroyParams).then(function () {
          $log.info('vehicle ' + vehicle.id + ' images removed');
        }, function (err) {
          $log.info('vehicle ' + vehicle.id + ' images error ', err);
        });

        Vehicle.destroy(vehicle.id).then(function () {
          ctrl.alerts.push({type: 'success', msg: 'Poistettu'});
          $timeout(function () {
            $state.go('app.manager');
          }, 500);
        }, function (err) {
          ctrl.alerts.push({type: 'danger', msg: 'Poistaminen epäonnistui'});
          $log.error(err);
        });
      }
    };

    this.disabled = function(date, mode) {
      return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    };

    this.addHistory = function() {
      var obj = {
        'vehicle_id': $scope.vehicle.id,
        date: moment().format('YYYY-MM-DD'),
        km: '',
        description: ''
      };
      History.create(obj).then(function() {
        $log.debug('history added to server :/');
        ctrl.alerts.push({ type: 'success', msg: 'Historia lisätty' });
      });
    };

    this.onSuccess = function(event) {

      _.each(event.fpfiles, createImage);

      function createImage(data) {
        var image = Image.createInstance();
        image.filename = data.filename;
        image.size = data.size;
        image.mimetype = data.mimetype;
        image.url = data.url;
        if (!$scope.vehicle.image) {
          $scope.vehicle.image = [];
        }
        $scope.vehicle.image.push(image);
      }
    };

    this.minDate = new Date(1970, 5, 22);
    this.maxDate = new Date(2020, 5, 22);
  })
;
