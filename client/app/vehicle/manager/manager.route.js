'use strict';
angular.module('vehicle.manager')
  .config(function($stateProvider) {
    $stateProvider
      .state('app.manager', {
        url: '/vehicle/manager',
        templateUrl: 'app/vehicle/manager/manager.html',
        controller: 'ManagerCtrl',
        resolve: {
          vehicles: function(Vehicle) {
            return Vehicle.findAll();
          }
        }
      })
      .state('app.manager.edit', {
        url: 'edit/:id',
        controllerAs: 'ctrl',
        resolve: {
          vehicle: function($q, $stateParams, Vehicle, $state, $log) {
            if (!$stateParams.id) {
              $log.warn('no id given for vehicle');
              return $q.reject();
            }
            return Vehicle.find($stateParams.id).then(function(vehicle) {
              return Vehicle.loadRelations(vehicle, ['history']).then(function() {
                return vehicle;
              });
            }, function(err) {
              console.error(err);
              $state.go('app.manager')
            });

          }
        },
        controller: function(vehicle) {
          this.vehicle = vehicle;
        },
        template: '<vehicle-editor vehicle="ctrl.vehicle"></vehicle-editor>'
      })
    ;
  })
;
