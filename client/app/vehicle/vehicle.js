'use strict';

angular.module('vehicle')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.vehicle', {
        url: '/vehicle',
        templateUrl: 'app/vehicle/vehicle.list.html',
        controller: 'VehicleCtrl',
        resolve: {
          vehicles: function(Vehicle) {
            return Vehicle.findAll();
          }
        }
      })
      .state('app.vehicle.view', {
        url: '/view/:id',
        template: '<vehicle-view vehicle="vehicle"></vehicle-view>',
        controller: function($scope, vehicle) {
          $scope.vehicle = vehicle;
        },
        resolve: {
          vehicle: function($stateParams, Vehicle) {
            return Vehicle.find($stateParams.id).then(function(vehicle) {
              return Vehicle.loadRelations(vehicle.id, ['history']);
            }, function(err) {
              throw err;
            });
          }
        }
      })
      .state('app.vehicle.slug', {
        url: '/:slug',
        template: '<vehicle-view-slug slug="slug"></vehicle-view-slug>',
        controller: function($scope, $stateParams) {
          $scope.slug = $stateParams.slug;
        }
      })
    ;
  });
