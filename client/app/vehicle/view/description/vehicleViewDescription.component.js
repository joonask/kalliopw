angular.module('vehicle').component('vehicleViewDescription', {
  templateUrl: 'app/vehicle/view/description/vehicleViewDescription.html',
  bindings: {
    vehicle: '<'
  }
});
