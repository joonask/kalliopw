angular.module('vehicle').component('vehicleView', {
  templateUrl: 'app/vehicle/view/vehicleView.html',
  bindings: {
    vehicle: '<'
  }
});
