angular.module('vehicle').component('vehicleViewService', {
  templateUrl: 'app/vehicle/view/service/vehicleViewService.html',
  bindings: {
    vehicle: '<'
  },
  controller: function() {
    var $ctrl = this;
    $ctrl.sort = 'date';
    $ctrl.order = true;
  }
});
