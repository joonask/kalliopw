angular.module('vehicle').component('vehicleViewImages', {
  templateUrl: 'app/vehicle/view/images/vehicleViewImages.html',
  bindings: {
    images: '<'
  }
});
