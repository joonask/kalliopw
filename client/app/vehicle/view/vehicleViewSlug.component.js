angular.module('vehicle').component('vehicleViewSlug', {
  templateUrl: 'app/vehicle/view/vehicleView.html',
  bindings: {
    slug: '<'
  },
  controller: function($q, $log, lodash, Vehicle) {
    console.log('slug view');
    var $ctrl = this;

    $ctrl.$onInit = function() {
      Vehicle.findAll({slug: $ctrl.slug}).then(function(res) {
        if (res.length > 1) {
          $log.warn('found more than one slugs');
        } else if (res.length === 0) {
          $log.warn('slug "' + $ctrl.slug + '" not found');
          return $q.reject('not found');
        }
        var vehicle = lodash.first(res);
        Vehicle.loadRelations(vehicle.id, ['history']).then(function() {
          $ctrl.vehicle = vehicle;
        });
      }, function(err) {
        $ctrl.error = err;
      });
    }
  }
});
