'use strict';

angular.module('vehicle')
  .factory('VehicleResource', function ($resource) {
    return $resource('/api/vehicle/:id', {id:'@id'});
  });

