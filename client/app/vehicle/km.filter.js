'use strict';

angular
  .module('vehicle')
  .filter('km', function() {
    /**
     * Return pretty number
     * @params input   - voucher code string
     * @params len     - length of character sets
     * @returns prettified number - string
     */
    return function(input) {
      var out = '';
      input = input.toString();
      while (input.length > 0) {
        out = ' ' + input.substr(-3) + out;
        input = input.substr(0, input.length-3);
      }
      return out.trim() + ' km';
    };
  })
;

