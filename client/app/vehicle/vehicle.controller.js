'use strict';

angular.module('vehicle')
  .controller('VehicleCtrl', function ($scope, vehicles) {
    $scope.vehicles = vehicles;
  });
