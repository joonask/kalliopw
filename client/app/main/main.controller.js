'use strict';

angular.module('qctusApp')
  .run(function($rootScope) {
      $rootScope.$on('$stateChangeError', function(err) {
        console.error('error', err);
      });
  })
  .controller('MainCtrl', function () {
  });

