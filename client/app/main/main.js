'use strict';

angular.module('qctusApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.main', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('app.bmw', {
        url: '/bmw',
        templateUrl: 'app/main/bmw.html',
        controller: 'MainCtrl'
      })
      .state('app.opel', {
        url: '/opel',
        templateUrl: 'app/main/opel.html',
        controller: 'MainCtrl'
      })
      .state('app.opel.takasatula', {
        url: '/takasatula',
        templateUrl: 'app/main/opel_astra_takasatula.html',
        controller: 'MainCtrl'
      })
      .state('app.ford', {
        url: '/ford',
        templateUrl: 'app/main/ford.html',
        controller: 'MainCtrl'
      })
      .state('app.yamaha', {
        url: '/yamaha',
        templateUrl: 'app/main/yamaha.html',
        controller: 'MainCtrl'
      })
      .state('app.logout', {
        url: '/logout',
        controller: function($state, DSFirebaseAdapter, $log) {
          DSFirebaseAdapter.ref.unauth();
          $log.debug('ref.unauth()');
          $state.go('app.main', {reload: true, notify: true});
        }
      })
    ;
  });
