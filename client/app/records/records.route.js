'use strict';

angular.module('records')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.records', {
        url: '/records',
        template: '<records-list></records-list>'
      })
      //.state('records.add', {
      //  url: '/records/add',
      //  template: '<records-add></records-add>'
      //})
    ;
  });
