angular.module('records')
  .controller('recordsAddModalController', function($uibModalInstance, $scope, track, profile) {
    var $ctrl = this;
    $ctrl.timePattern = /^\d\d:\d\d:\d\d\d$/;
    $ctrl.record = {
      name: profile.name,
      trackId: track.id
    };
    $ctrl.track = track;

    $ctrl.ok = function () {
      if ($scope.recordForm.$valid) {
        $uibModalInstance.close($ctrl.record);
      }
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
