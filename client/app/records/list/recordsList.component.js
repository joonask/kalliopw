angular.module('records')
  .component('recordsList', {
    templateUrl: 'app/records/list/list.html',
    controller: function(recordsService, tracksService, $uibModal, $log, auth) {
      var $ctrl = this;
      $ctrl.profile = auth.profile();
      $ctrl.alerts = [];
      $ctrl.currentTrack = undefined;
      tracksService.getAll()
        .then(function(tracks) {
          $ctrl.tracks = tracks;
          setTrack(tracks[0].id);
        });

      $ctrl.addRecord = function() {
        var modalInstance = $uibModal.open({
          controller: 'recordsAddModalController',
          controllerAs: '$ctrl',
          templateUrl: 'app/records/add/add.html',
          resolve: {
            track: $ctrl.currentTrack,
            profile: $ctrl.profile
          }
        });

        modalInstance.result.then(function (recordData) {
          recordsService.add(recordData)
            .then(function(record) {
              $ctrl.alerts.push({msg:'Tallennettu uusi aika', type: 'success'});
              $ctrl.records.push(record);
            }, function(err) {
              $ctrl.alerts.push({msg:'Tallennus epäonnistui', type: 'danger'});
            });
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $ctrl.addTrack = function() {
        var modalInstance = $uibModal.open({
          controller: 'tracksAddModalController',
          controllerAs: '$ctrl',
          templateUrl: 'app/records/tracks/add/add.html'
        });

        modalInstance.result.then(function (trackData) {
          tracksService.add(trackData)
            .then(function(track) {
              $ctrl.alerts.push({msg:'Tallennettu uusi rata', type: 'success'});
              $ctrl.tracks.push(track);
            }, function(err) {
              $ctrl.alerts.push({msg:'Tallennus epäonnistui', type: 'danger'});
              $log.warn('track save error', err);
            });
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $ctrl.track = function(id) {
        setTrack(id);
      };

      $ctrl.closeAlert = function(index) {
        $ctrl.alerts.splice(index, 1);
      };

      function setTrack(id) {
        tracksService.get(id)
          .then(function(track) {
            $ctrl.currentTrack = track;
            recordsService.getAll({trackId: track.id})
              .then(function(records) {
                $ctrl.records = _.map(records);
              }, function() {
                $ctrl.alerts.push({msg:'Ei käyttöoikeuksia', type: 'warning'});
                $ctrl.records = [];
              });
          })
      }
    }
  })
  .filter('recordString', function() {
    return function(milliseconds) {
      var minutes = Math.floor(milliseconds / 1000 / 60);
      milliseconds -= Math.floor(minutes * 1000 * 60);
      var seconds = Math.floor(milliseconds / 1000);
      milliseconds -= Math.floor(seconds * 1000);

      return zeroPrefix(minutes, 2) +':' + zeroPrefix(seconds, 2) + ':' + zeroPrefix(milliseconds, 3)

      function zeroPrefix(number, len) {
        return '0'.repeat(len-number.toString().length)+number;
      }
    }
  });
