'use strict';

angular.module('records')
  .service('recordsService', function(Record) {
    return {
      getAll: function(params) {
        return Record.findAll(params);
      },
      add: function(record) {
        var regex = /^(\d\d):(\d\d):(\d\d\d)$/g;
        var str = record.record;
        var m = regex.exec(str);

        var minutes = parseInt(m[1], 10);
        var seconds = parseInt(m[2], 10);
        var mseconds = parseInt(m[3], 10);

        var total = minutes * 60 * 1000;
        total += seconds * 1000;
        total += mseconds;
        record.record = total;
        record.date = new Date().toISOString();
        return Record.create(record);
      }
    };
  });
