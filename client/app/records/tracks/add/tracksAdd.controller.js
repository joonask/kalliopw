angular.module('tracks')
  .controller('tracksAddModalController', function($uibModalInstance, $scope) {
    var $ctrl = this;
    $ctrl.track = {};

    $ctrl.ok = function () {
      if ($scope.trackForm.$valid) {
        $uibModalInstance.close($ctrl.track);
      }
    };

    $ctrl.cancel = function () {
      $uibModalInstance.dismiss('cancel');
    };
  });
