'use strict';

angular.module('tracks')
  .config(function ($stateProvider) {
    $stateProvider
      .state('app.tracks', {
        url: '/tracks',
        template: '<tracks-list></tracks-list>'
      })
      //.state('records.add', {
      //  url: '/records/add',
      //  template: '<records-add></records-add>'
      //})
    ;
  });
