angular.module('tracks')
  .component('tracksList', {
    templateUrl: 'app/records/tracks/list/list.html',
    controller: function(tracksService, $uibModal, $log) {
      var $ctrl = this;
      $ctrl.alerts = [];
      tracksService.getAll()
        .then(function(tracks) {
          $ctrl.tracks = _.map(tracks);
        }, function() {
          $ctrl.alerts.push({msg:'Ei käyttöoikeuksia', type: 'warning'});
          $ctrl.tracks = [];
        });

      $ctrl.add = function() {
        var modalInstance = $uibModal.open({
          controller: 'tracksAddModalController',
          controllerAs: '$ctrl',
          templateUrl: 'app/records/tracks/add/add.html'
        });

        modalInstance.result.then(function (data) {
          tracksService.add(data)
            .then(function(data) {
              $ctrl.alerts.push({msg:'Tallennettu uusi rata', type: 'success'});
              $ctrl.tracks.push(data);
            }, function(err) {
              $ctrl.alerts.push({msg:'Tallennus epäonnistui', type: 'danger'});
            });
        }, function () {
          $log.info('Modal dismissed at: ' + new Date());
        });
      };

      $ctrl.closeAlert = function(index) {
        $ctrl.alerts.splice(index, 1);
      }
    }
  });
