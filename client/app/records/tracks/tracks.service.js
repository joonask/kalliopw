'use strict';

angular.module('tracks')
  .service('tracksService', function(Track) {
    return {
      get: function(id) {
        return Track.find(id);
      },
      save: function(track) {
        if (track.id) {
          return Track.save(track.id);
        } else {
          return Track.create(track);
        }
      },
      getAll: function() {
        return Track.findAll();
      },
      add: function(data) {
        return Track.create(data);
      }
    };
  });
