'use strict';
angular.module('app.auth', ['app.models', 'app.auth.google'])
  .factory('auth', function(googleAuth, User) {
    var _profile;
    return {
      getProfile: function() {
        var that = this;
        return googleAuth.initialize()
          .then(function(authData) {
            if (!authData) {
              console.log('!authData, not logged in');
              return false;
            }
            return User.find(authData.auth.uid).then(function( ) {
              that.login(authData.auth.uid, authData.google.displayName);
              return that.profile;
            }, function() {
              User.create({uid: authData.auth.uid, name: authData.google.displayName, provider: 'google'}).then(function() {
                that.login(authData.auth.uid, authData.google.displayName);
                return that.profile;
              }, function(err) {
                console.error('create user failed!', err);
                that.logout;
              });
            });
          }, function(err) {
            // err not logged in
            console.error('google auth error', err);
          } );
      },
      login: function(id, name) {
        _profile = {id: id, name: name};
      },
      logout: function() {
        _profile = undefined;
      },
      profile: function() {
        return _profile;
      }
    };
  });
