'use strict';

angular.module('app.auth.google', ['js-data'])

  .config(function (DSFirebaseAdapterProvider) {
    var basePath = 'https://flickering-inferno-3146.firebaseio.com/';
    DSFirebaseAdapterProvider.defaults.basePath = basePath;
  })
  .factory('googleAuth', function($q, DS, DSFirebaseAdapter) {
    DS.registerAdapter('firebase', DSFirebaseAdapter, { default: true });
    return {
      initialize: function() {
        console.log('google.auth init');
        var defer = $q.defer();
        var ref = DSFirebaseAdapter.ref;

        ref.onAuth(function(authData) {
          if (authData) {
            console.log('found authData', authData);
            defer.resolve(authData);
          } else {
            console.log('not found');
            defer.resolve();
          }
        });
        return defer.promise;
      }
    }
  })
;
