'use strict';

angular.module('qctusApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'vehicle',
  'asunto',
  'ngLodash',
  'calculator',
  'utils',
  'demo',
  'app.auth',
  'records'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider
      .state('app', {
        abstract: true,
        template: '<initialize ng-if="!initialized"></initialize><ui-view ng-if="initialized"></ui-view>',
        resolve: {
          profile: function(auth) {
            return auth.getProfile();
          }
        },
        controller: function($scope) {
          $scope.initialized = true;
        }
      });
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
