'use strict';

angular.module('qctusApp')
  .directive('navbar', function() {
    return {
      templateUrl: 'components/navbar/navbar.html',
      restrict: 'E',
      controller: 'NavbarController as nav'
    };
  });
