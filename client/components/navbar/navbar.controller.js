'use strict';

angular.module('qctusApp')
  .controller('NavbarController', function ($scope, $stateParams, auth, DSFirebaseAdapter, $log) {
    var ctrl = this;

    ctrl.mopo = 1;
    ctrl.profile = auth.profile();


    function updateMenu() {
      ctrl.menu = [
        {
          'title': 'Asunto',
          'link': '/asunto'
        }
      ];
      if (ctrl.profile) {
        ctrl.menu.push({title: 'Ajoneuvot', 'link': '/vehicle'});
        ctrl.menu.push({title: 'Muokkaus', link: '/vehicle/manager'});
        ctrl.menu.push({title: 'Polttoainelaskuri', link: '/polttoainelaskuri'});
        ctrl.menu.push({title: 'Kierrosajat', link: '/records'});
      }
    }


    $scope.$watch(function() {
      return auth.profile();
    }, function(changed) {
      ctrl.profile = changed;
      updateMenu();
    });


    ctrl.isCollapsed = true;
    updateMenu();

    var ref = DSFirebaseAdapter.ref;

    ctrl.login = function() {
      ref.authWithOAuthRedirect('google', function(error) {
        if (error) {
          $log.debug('Authentication failed:', error);
        }
      });
    };

    ctrl.isActive = function(id) {
      return id === $stateParams.id;
    };
  });
