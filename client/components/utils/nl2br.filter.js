'use strict';

angular.module('utils')
  .filter('nl2br', function() {
    return function(input) {
      if (input !== void 0) {
        return input.replace(/\n/g, '<br>');
      }
    };
  })
  .filter('br2nl', function() {
    return function(input) {
      if (input !== void 0) {
        return input.replace(/<br>/g, "\n");
      }
    };
  });;

