'use strict';

angular.module('qctusApp')
  .directive('hoverimage', function ($log) {
    return {
      templateUrl: 'components/hoverimage/hoverimage.html',
      restrict: 'EA',
      transclude: true,
      scope: {
        altText: '@',
        path: '@',
        description: '@'
      },
      link: function (scope, element) {
        element.addClass('show-hand');
        element.bind('click', function() {
          $log.debug('mouse click', scope.showImage);
          var imgEl = element.find('div.hoverimage');
          if (imgEl.hasClass('show-image')) {
            $log.debug('visible');
            imgEl.removeClass('show-image');
          } else {
            $log.debug('not visible');
            imgEl.addClass('show-image');
          }
        });
      }
    };
  });
