'use strict';

describe('Directive: hoverimage', function () {

  // load the directive's module and view
  beforeEach(module('qctusApp'));
  beforeEach(module('components/hoverimage/hoverimage.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<hoverimage></hoverimage>');
    element = $compile(element)(scope);
    scope.$apply();
    expect(element.text()).toBe('this is the hoverimage directive');
  }));
});