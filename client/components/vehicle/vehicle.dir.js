'use strict';

angular.module('qctusApp')
  .directive('vehicle', function() {
    return {
      restrict: 'E',
      scope: {
        data: '=',
        url: '@url'
      },
      templateUrl: 'components/vehicle/vehicle.dir.html'
    };
  });
