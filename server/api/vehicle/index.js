'use strict';

var express = require('express');
var controller = require('./vehicle.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/:id', controller.details);

module.exports = router;