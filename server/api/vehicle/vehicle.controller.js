'use strict';

var _ = require('lodash');
var fs = require('fs');

var vehicles = [
  {key: 'bmw', file: 'bmw.json'},
  {key: 'opel', file: 'opel.json'},
  {key: 'ford', file: 'ford.json'},
  {key: 'yamaha', file: 'yamaha.json'},
  {key: 'kona', file: 'kona.json'},
  {key: 'focus', file: 'focus.json'}
];

// Get list of vehicles
exports.index = function(req, res) {
  res.json(vehicles.map(function(v) { return {id: v.key}}));
};

exports.details = function(req, res) {

  var index = _.findIndex(vehicles, function(chr) {
    return chr.key === req.params.id;
  });

  if (index >= 0) {
    fs.readFile(__dirname + '/data/' + vehicles[index].file, 'utf8', function (err, data) {
      if (err) throw err;
      res.json(JSON.parse(data));
    });
  } else {
    res.status(404).json({error: "not found"});
  }

};
